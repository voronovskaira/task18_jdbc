package db;

import constant.Query;
import model.Artist;
import model.MusicLabel;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class ArtistDao implements Dao<Artist> {
    private Connection connection;
    private MusicLabelDao labelDao = new MusicLabelDao();

    @Override
        public Artist get(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.GET_ARTIST_QUERY);
        statement.setLong(1, id);
        ResultSet rs = statement.executeQuery();
        Artist artist = new Artist();
        while (rs.next()) {
            artist.setName(rs.getString("name"));
            artist.setMusicLabel(new MusicLabel(rs.getString("label_name")));
        }
        return artist;
    }

    @Override
    public Map<Integer, String> getAll() throws SQLException {
        connection = ConnectionManager.getConnection();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(Query.GET_ALL_ARTIST_QUERY);
        Map<Integer, String> map = new HashMap<>();
        while (rs.next()) {
            map.put(rs.getInt("id"), rs.getString("name"));
        }
        return map;
    }

    @Override
    public void create(Artist artist) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.ADD_ARTIST_QUERY);
        statement.setString(1, artist.getName());
        statement.setInt(2, labelDao.getLabelId(artist.getMusicLabel()));
        statement.execute();
    }

    @Override
    public void update(Artist artist, int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.UPDATE_ARTIST_QUERY);
        statement.setString(1, artist.getName());
        statement.setLong(2, id);
        statement.execute();
    }

    @Override
    public void delete(int id) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.DELETE_ARTIST_QUERY);
        statement.setLong(1, id);
        statement.execute();
    }

    public int getId(Artist artist) throws SQLException {
        connection = ConnectionManager.getConnection();
        PreparedStatement statement = connection.prepareStatement(Query.FIND_ARTIST_ID_QUERY);
        statement.setString(1, artist.getName());
        ResultSet rs = statement.executeQuery();
        rs.next();
        return rs.getInt("id");
    }
}
