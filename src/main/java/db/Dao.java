package db;

import java.sql.SQLException;
import java.util.Map;

public interface Dao<T> {
    T get(int id) throws SQLException;

    Map<Integer, String> getAll() throws SQLException;

    void create(T t) throws SQLException;

    void update(T t, int id) throws SQLException;

    void delete(int id) throws SQLException;
}
