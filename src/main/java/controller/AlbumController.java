package controller;

import constant.MenuMessages;
import db.AlbumDao;
import db.ArtistDao;
import db.Dao;
import model.Album;
import model.Artist;
import util.ConsoleReader;
import util.MenuOptions;
import view.View;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class AlbumController extends Controller<Album> {
    private Dao<Album> albumDao = new AlbumDao();
    private Dao<Artist> artistDao = new ArtistDao();
    private Controller<Artist> artistController;
    private View view;
    private ConsoleReader consoleReader;

    public AlbumController(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
        this.artistController = new ArtistController(view, consoleReader);

    }

    @Override
    public Album get() {
        Album album = new Album();
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose album to view info", map.keySet());
            album = albumDao.get(userChoice);
        } catch (SQLException e) {
            LOG.error("Couldn't get album");
        }
        return album;

    }

    @Override
    public Album create() {
        Album album = new Album();
        try {
            album.setName(consoleReader.scanFromConsole("Please input album name"));
            selectArtist(album);
            albumDao.create(album);
            view.print(MenuMessages.RECORD_ADDED + album);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return album;
    }

    @Override
    public Map<Integer, String> getAll() {
        Map<Integer, String> map = new HashMap<>();
        try {
            map = albumDao.getAll();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return map;

    }

    @Override
    public void update() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose album to update", map.keySet());
            Album album = new Album();
            album.setName(consoleReader.scanFromConsole("Please input album name"));
            selectArtist(album);
            albumDao.update(album, userChoice);
            view.print(MenuMessages.RECORD_UPDATED + album);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

    }

    @Override
    public void delete() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose album to delete", map.keySet());
            albumDao.delete(userChoice);
            view.print(MenuMessages.RECORD_DELETED);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

    }

    private void selectArtist(Album album) throws SQLException {
        Map<Integer, String> map = artistDao.getAll();
        view.print(MenuOptions.showMenuAddNewRecord(map));
        int artistId = consoleReader.readValue("Choose artist", map.keySet());
        if (artistId == 0) {
            Artist artist = artistController.create();
            album.setArtist(artist);
        } else
            album.setArtist(artistDao.get(artistId));
    }
}

