package controller;

import constant.MenuMessages;
import db.ArtistDao;
import db.Dao;
import db.MusicLabelDao;
import model.Artist;
import model.MusicLabel;
import util.ConsoleReader;
import util.MenuOptions;
import view.View;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ArtistController extends Controller<Artist> {

    private Dao<Artist> artistDao = new ArtistDao();
    private Dao<MusicLabel> labelDao = new MusicLabelDao();
    private View view;
    private ConsoleReader consoleReader;

    public ArtistController(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
    }

    public Artist get() {
        Artist artist = new Artist();
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose artist to view info", map.keySet());
            artist = artistDao.get(userChoice);
        } catch (SQLException e) {
            LOG.error("Couldn't get music label");
        }
        return artist;
    }

    @Override
    public Artist create() {
        Artist artist = new Artist();
        try {
            artist.setName(consoleReader.scanFromConsole("Please input artist name"));
            selectMusicLabel(artist);
            artistDao.create(artist);
            view.print(MenuMessages.RECORD_ADDED + artist);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return artist;
    }

    @Override
    public Map<Integer, String> getAll() {
        Map<Integer, String> map = new HashMap<>();
        try {
            map = artistDao.getAll();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return map;
    }

    @Override
    public void update() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose artist to update", map.keySet());
            Artist artist = new Artist();
            artist.setName(consoleReader.scanFromConsole("Please input artist name"));
            selectMusicLabel(artist);
            artistDao.update(artist, userChoice);
            view.print(MenuMessages.RECORD_UPDATED + artist);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void delete() {
        try {
            Map<Integer, String> map = getAll();
            view.print(MenuOptions.showMenu(map));
            int userChoice = consoleReader.readValue("Choose artist to delete", map.keySet());
            artistDao.delete(userChoice);
            view.print(MenuMessages.RECORD_DELETED);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    private void selectMusicLabel(Artist artist) throws SQLException {
        Map<Integer, String> map = labelDao.getAll();
        view.print(MenuOptions.showMenuAddNewRecord(map));
        int labelId = consoleReader.readValue("Choose music label of artist", map.keySet());
        if (labelId == 0) {
            String labelName = consoleReader.scanFromConsole(MenuMessages.INPUT_MUSIC_LABEL_NAME);
            MusicLabel label = new MusicLabel(labelName);
            labelDao.create(new MusicLabel(labelName));
            artist.setMusicLabel(label);
        } else
            artist.setMusicLabel(new MusicLabel(map.get(labelId)));
    }
}


