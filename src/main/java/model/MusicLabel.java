package model;

public class MusicLabel {
    String name;

    public MusicLabel(String name) {
        this.name = name;
    }

    public MusicLabel(){

    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "MusicLabel{" +
                "name='" + name + '\'' +
                '}';
    }
}
