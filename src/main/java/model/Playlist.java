package model;

public class Playlist {
    String name;
    User user;

    public Playlist(String name, User user) {
        this.name = name;
        this.user = user;
    }
}
