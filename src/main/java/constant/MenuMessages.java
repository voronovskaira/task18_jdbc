package constant;

public class MenuMessages {

    public static String FIRST_MESSAGE = "Hello, Please choose table to modify";
    public static String RECORD_ADDED = "Record successfully added: ";
    public static String RECORD_UPDATED = "Record successfully updated: ";
    public static String RECORD_DELETED = "Record successfully deleted";
    public static String INPUT_MUSIC_LABEL_NAME = "Please input music label name";
    public static String INPUT_ARTIST_NAME = "Please input artist name";
}
