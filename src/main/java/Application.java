import menu.MainMenu;
import view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        new MainMenu(new ConsoleView()).run();
    }
}
