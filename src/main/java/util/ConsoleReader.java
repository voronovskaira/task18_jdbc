package util;

import view.View;

import java.util.Scanner;
import java.util.Set;

public class ConsoleReader {

    private View view;

    public ConsoleReader(View view) {
        this.view = view;
    }

    public String scanFromConsole(String message) {
        Scanner scan = new Scanner(System.in);
        view.print(message);
        return scan.nextLine();
    }

    public int readValue(String message, Set<Integer> keys) {
        try {
            int result = Integer.parseInt(scanFromConsole(message));
            if (checkRange(result, keys))
                return result;
        } catch (NumberFormatException e) {
            view.print("incorrect value");
        }
        return readValue(message, keys);
    }

    private boolean checkRange(int value, Set<Integer> keys) throws NumberFormatException {
        if (keys.contains(value)) {
            return true;
        } else throw new NumberFormatException();
    }
    public int readValue(String message) {
        try {
            int result = Integer.parseInt(scanFromConsole(message));
            if (result>0)
                return result;
        } catch (NumberFormatException e) {
            view.print("Incorrect value");
        }
        return readValue(message);
    }

}
